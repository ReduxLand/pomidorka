$(document).ready(function(){
	var clock = $('#clock span:eq(0)');
	var workTime = 25;
	var restTime = 5;
	var doubleRestTime = 10;
	var workStatus = true;
	var paused = false;
	var temp = 0;
	var loop = 0;
	var startAlert = document.createElement('audio');
	var stopAlert = document.createElement('audio');
        startAlert.setAttribute('src', 'audio/start.wav');
	stopAlert.setAttribute('src', 'audio/stop.wav');
	startAlert.load();
	stopAlert.load();
	clock.countdown(new Date().getTime() + workTime * 60 * 1000, {elapse: true})
		.on('update.countdown', function(event){
			if (event.elapsed && workStatus){
				//if (loop % 4 == 0)
					//$(this).countdown(new Date().getTime() + doubleRestTime * 60 * 1000);
				//else
					$(this).countdown(new Date().getTime() + restTime * 60 * 1000);
				stopAlert.play();
				workStatus = false;
				//loop++;
			} else if (event.elapsed && !workStatus){
				$(this).countdown(new Date().getTime() + workTime * 60 * 1000);
				startAlert.play();
				workStatus = true;	
			} else {
				$(this).html(event.strftime("%M:%S"));
				temp = (event.offset.minutes * 60 + event.offset.seconds) * 1000;
			}
	}).click(function(){
		if(paused){
			$(this).countdown(new Date().getTime() + temp);
			$(this).countdown('start');
			paused = false;
		} else {
			$(this).countdown('stop');
			paused = true;
		}	
	});
	$('#restart-work').click(function(){
		clock.countdown(new Date().getTime() + workTime * 60 * 1000);
		startAlert.play();
	});
	$('#have-a-rest').click(function(){
		clock.countdown(new Date().getTime() + restTime * 60 * 1000);
		stopAlert.play();
	});
});